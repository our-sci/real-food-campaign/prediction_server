library(tidyverse)
library(ncdf4)
library(parallel)

## https://www.climatologylab.org/terraclimate-variables.html

## TErraClimate variables
## The following variables are provided for download as 30-year climatological monthly summaries or monthly data for each year (1958-present). Data are available on NKN THREDDS servers or through Google Earth Engine. Note that data come as compressed netCDF. Be sure to reference scale_factor and offset commands when looking at the data.

## aet (Actual Evapotranspiration, monthly total), units = mm
## def (Climate Water Deficit, monthly total), units = mm
## pet (Potential evapotranspiration, monthly total), units = mm
## ppt (Precipitation, monthly total), units = mm
## q (Runoff, monthly total), units = mm
## soil (Soil Moisture, total column - at end of month), units = mm
## srad (Downward surface shortwave radiation), units = W/m2
## swe (Snow water equivalent - at end of month), units = mm
## tmax (Max Temperature, average for month), units = C
## tmin (Min Temperature, average for month), units = C
## vap (Vapor pressure, average for month), units  = kPa
## ws (Wind speed, average for month), units = m/s
## vpd (Vapor Pressure Deficit, average for month), units = kpa
## PDSI (Palmer Drought Severity Index, at end of month), units = unitless


retrieveTerraclimateData <- function( variable, site_long, site_lat ) {
  coordinates_pair <- list(
    lon=site_long,
    lat=site_lat
  )

  baseurlagg <- paste0("http://thredds.northwestknowledge.net:8080/thredds/dodsC/agg_terraclimate_",variable,"_1958_CurrentYear_GLOBE.nc")

  ## starts the remote file stream
  nc <- nc_open(baseurlagg)

  ## retrieves lists of indexed latitudes and longitudes on the DB.
  lon <- ncvar_get(nc, "lon")
  lat <- ncvar_get(nc, "lat")
  coordinates_dataset_precision <- 1/48

  ## find indexed coordinate closer to provided coordinate
  latindex = which( abs(lat - coordinates_pair$lat) < coordinates_dataset_precision )

  ## find indexed coordinate closer to provided coordinate
  lonindex = which( abs(lon - coordinates_pair$lon) < coordinates_dataset_precision)

  ## this describes the shape (dimensions) of the expected object. -1 is a special value telling it to just grab all, as we may not know how many days are available
  start <- c(lonindex, latindex, 1)
  count <- c(1, 1, -1)

  # read in the full period of record using aggregated files
  data <- as.numeric(ncvar_get(nc, varid = variable,start = start, count))

  # let's turn it into a table
  dataMatrix <- matrix( data = data, ncol=12)
  dimnames( dataMatrix ) <- list( seq(from=1958, length.out=nrow(dataMatrix)), month.name )
  table <- dataMatrix %>%
    as_tibble( rownames = "year" )

  return( table )
}

getOnboardingClimateDataParallel <- function( variables = c("tmin","tmax","ppt"), site_long, site_lat ) {
  tasks <- map(
    variables,
    ~ mcparallel( retrieveTerraclimateData( variable = .x, site_long = site_long, site_lat = site_lat ), name=.x )
  )
  tables <- mccollect( tasks )
  return(tables)
}

## exampleMichigan <- getOnboardingClimateDataParallel( site_long =  -84.4582943055987, site_lat = 42.799786976106724 )
## exampleMichigan$ppt %>% filter( year %in% 1971:2000 ) %>% summarize( across(!starts_with("year"), ~ mean(.x) / 25.4 )  )
## exampleDetroit <- getOnboardingClimateDataParallel( site_long =  -83.04575, site_lat = 42.33143 )
## exampleDetroit$ppt %>% filter( year %in% 1971:2000 ) %>% summarize( across(!starts_with("year"), ~ mean(.x) / 25.4 )  )
