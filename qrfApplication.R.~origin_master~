library(RestRserve)
library(jsonlite)
library(dplyr)
library(readr)
library(tibble)
library(purrr)
library(stringr)
library(ggplot2)
library(readr)
library(quantregForest)

sanitizeQuantilesNames <- function( tibble ) {
  tibble %>%
    rename_at(
      vars( contains("quantile") ),
      .funs = ~ str_c( "centile",
                      as.double( str_extract( .x, pattern = "0[:punct:]{1}[:alnum:]*" ) ) * 100
                      )
    )
}

#loading the model
models <- read_rds("./models/deployModels.Rds")
transitionalModel <- read_rds("./models/transitionalModel.Rds")
histogramsData <- read_rds( "./tables/histogramsData.Rds" )
testData <- read_csv( "./tables/testData.csv", guess_max = 4500 )

allWavelengths <- transitionalModel$allWavelengths
allTransitionalModels <- transitionalModel$allTransitionalModels
predictAWavelength <- transitionalModel$predictAWavelength
predictEachWavelength <- transitionalModel$predictEachWavelength

simple_post = function(.req, .res) {
  inputX = .req$body$x
  inputY = .req$body$y

  result = list( x = inputX, y = inputY, z = inputX+inputY )

  .res$content_type = "application/json"
  .res$set_body(result)
}

simple_get = function(.req, .res) {
  .res$content_type = "text/plain"
  .res$set_body( "working" )
}

testVectorGrain <- testData %>%
  filter( Type == "oats" )
testVectorGrain <- testVectorGrain %>%
  slice( sample( 1:nrow(testVectorGrain), 1 ) ) %>%
  mutate(
    dataset = "c-no-vis-who",
    ) %>%
  select( contains("whole.scan"), "Type", dataset ) %>%
  rename_all( ~ str_replace( .x, pattern = "whole.scan", replacement = "surface.scan" ) )

testVectorProduce <- testData %>%
  filter( Type == "beet" )
testVectorProduce <- testVectorProduce %>%
  slice( sample( 1:nrow(testVectorProduce), 1 ) ) %>%
  mutate(
    dataset = "c-no-vis-who",
    ) %>%
  select( contains( "color."), contains("surface.scan"), "Type", dataset )

## transitionTest <- testVector %>%
##   select( contains( "surface.scan" ) ) %>%
##   rename_all( ~ str_replace( string = .x, pattern = "surface.scan_", replacement = "barium_standard_" ) )

## transitionResult <- predictEachWavelength( transitionalModelsList = allTransitionalModels, newdata = transitionTest )


applyTransitionalModel <- function( vectorFromField, wavelengthsGroupPattern, patternExpectedByTransitionalModel = "barium_standard_" ) {
  yVector <- vectorFromField %>%
    select( matches(wavelengthsGroupPattern) ) %>%
    rename_all( ~ str_replace( string = .x, pattern = wavelengthsGroupPattern, replacement = patternExpectedByTransitionalModel ) )
  xVector <- predictEachWavelength( transitionalModelsList = allTransitionalModels, newdata = yVector ) %>%
    unlist %>%
    t(.) %>%
    as_tibble() %>%
    rename_all( ~ str_c( wavelengthsGroupPattern, .x ) )

  output <- bind_cols(
    vectorFromField %>% select( - contains( wavelengthsGroupPattern ) ),
    xVector
  )
    return(output)
};

## case produce

## fieldVectorProduce <- testData %>%
##   filter( Type == "carrot" )
## fieldVectorProduce <- fieldVector %>%
##   slice( sample( 1:nrow(fieldVector), 1 ) ) %>%
##   mutate(
##     dataset = "c-no-vis-who",
##     )
## transitionedTestVectorProduce <- applyTransitionalModel( vectorFromField = fieldVectorProduce, wavelengthsGroupPattern = "surface.scan_" )

## case grain

## fieldVectorGrain <- testData %>%
##   filter( Type == "wheat" )
## fieldVectorGrain <- fieldVector %>%
##   slice( sample( 1:nrow(fieldVector), 1 ) ) %>%
##   mutate(
##     dataset = "c-no-vis-who",
##     )
## transitionedTestVectorGrain <- applyTransitionalModel( vectorFromField = fieldVectorGrain, wavelengthsGroupPattern = "whole.scan_" )

## failedEntry <- list(
##   Type= "carrot",
##   "color.beige"= FALSE,
##   "color.black"= FALSE,
##   "color.blue"= FALSE,
##   "color.brown"= FALSE,
##   "color.green"= FALSE,
##   "color.orange"= FALSE,
##   "color.purple"= TRUE,
##   "color.red"= FALSE,
##   "color.white"= FALSE,
##   "color.yellow"= FALSE,
##   dataset= "c-no-vis-who",
##   "surface.scan_365"= 776.6763333333333,
##   "surface.scan_385"= 742.11,
##   "surface.scan_450"= 897.9123333333333,
##   "surface.scan_500"= 877.8069999999999,
##   "surface.scan_530"= 1033.8519999999999,
##   "surface.scan_587"= 1172.9713333333332,
##   "surface.scan_632"= 1185.4936666666665,
##   "surface.scan_850"= 1807.6066666666666,
##   "surface.scan_880"= 1244.848,
##   "surface.scan_940"= 2915.9933333333333
## )

## failedTransitioned <- failedEntry %>%
##   as_tibble %>%
##   applyTransitionalModel( vectorFromField = .,
##                          wavelengthsGroupPattern = "surface.scan_"
##                          ) %>%
##   rename_at( vars(contains("color")), ~ str_c( .x, "TRUE" ) )

## predictions <- plotPredictions(failedTransitioned, plot = FALSE )


qrf_numbers = function(.req, .res) {
  input = .req$body
  newData <- as_tibble( input )

  namingPatternForWavelengths <- "surface.scan_"

  newData <- newData %>%
    applyTransitionalModel( vectorFromField = ., wavelengthsGroupPattern = namingPatternForWavelengths ) %>% rename_at( vars( contains("color") ), ~ str_c( .x, "TRUE" ) )

  if ( newData$Type %in% c("oats", "wheat") ) {
   newData <- newData %>% rename_all( ~ str_replace( .x, pattern = "surface.scan_", replacement = "whole.scan_" ) )
  }

  table <- plotPredictions( newData, plot = FALSE )

  table2 <- map( 1:nrow(table), ~ table[.x,] )

  .res$body = table2
  .res$content_type = "application/json"
}

qrf_random = function(.req, .res) {

  testVector <- testData %>%
    filter( Type == "peppers" )

  testVector <- testVector %>%
    slice( sample( 1:nrow(testVector), 1 ) ) %>%
    mutate(
      dataset = "c-no-vis-who",
      )
  newData <- testVector 

  ## print(newData)
  table <- plotPredictions( newData, plot = FALSE )

  table2 <- map( 1:nrow(table), ~ table[.x,] )
  ## print("table2")
  ## print(table2)

  ## print("table")
  ## print(table)


  .res$body = table2
  .res$content_type = "application/json"
}

## qrf_predict = function(.req, .res) {
##   input = .req$body[[1]]
##   newData <- as_tibble( input )
##   tmp = tempfile(fileext = ".png")

##   plot <- plotPredictions( newData )
##   ggsave(tmp, plot, "png")

##   ## .res$content_type = "image/png"
##   .res$set_body( c( "tmpfile" = tmp ) )
## }


plotPredictions <- function( newData, plot = TRUE ) {

  print("in function")

  chosen_models_table <- models %>%
    filter( species == newData$Type,
           dataset == newData$dataset )

  resultsTable <- tibble()
  histogramsTable <- tibble()
  chosen_model_row  <- tibble()


  for ( index in 1:nrow( chosen_models_table ) ) {


    chosen_model_row <- chosen_models_table %>%
      slice( index )

    chosen_model <- chosen_model_row %>%
      .["finalModel"] %>%
      .[[1]] %>%
      .[[1]]

    chosen_ecdf <- chosen_model_row %>%
      .["ecdf"] %>%
      .[[1]] %>%
      .[[1]] %>%
      .$ecdf

    if ( sum( str_detect( chosen_model$xNames, "cropColor" ) ) > 0 ) {
      ## print("detected")
      ## print(chosen_model$xNames)
      colorFactors <- chosen_model$xNames %>%
        keep( str_detect( ., "cropColor" ) ) %>%
        unlist
      ## print("color factors")
      ## print(colorFactors)

      for ( factor in colorFactors ) {
        newData[[factor]] <- FALSE
      }

      trueFactor <- colorFactors %>%
        keep( . == paste0( "cropColor", newData$cropColor ) )

      ## print( "trueFactor" )
      ## print( trueFactor )

      if ( length( trueFactor ) >= 1 ) {
        newData[[trueFactor]] <- TRUE
      }
    }

    newData<- newData[chosen_model$xNames]

    result <- predict( chosen_model , newData) %>%
      as_tibble() %>%
      sanitizeQuantilesNames() %>%
      rename(
        max = centile90,
        min = centile10,
        prediction = centile50,
        ) %>%
      mutate(
        max = max %>% chosen_ecdf,
        min = min %>% chosen_ecdf,
        prediction = prediction %>% chosen_ecdf,
        confidence=80
        ) %>%
      mutate( variable = rep( chosen_model_row$explained ) )

    resultsTable <- bind_rows( resultsTable, result )

    currentHistogram <- histogramsData %>%
      filter(
        Type == chosen_model_row$species,
        ) %>%
      select(
        chosen_model_row$explained
      ) %>%
      na.omit %>%
      mutate(
        explained = chosen_model_row$explained
      ) %>%
      rename( x = .data[[chosen_model_row$explained]] )
    histogramsTable <- bind_rows( histogramsTable, currentHistogram )
  }


  if (plot == FALSE) {
    print("sending table")
    return(resultsTable)
  }

  print("getting here")


  plot <- ggplot() +
    geom_histogram( data = histogramsTable, aes( x = x, fill = ..ncount.. ) ) +
    geom_errorbarh( data = resultsTable, aes( xmax = xmax, xmin = xmin, y = 0, height = 0.3, size = 1.5), color = "purple", alpha = 0.6  ) +
    geom_point( data = resultsTable, aes(x = x, y = 0 ) ) +
    geom_text( data = resultsTable, aes( x = x, y = -0.2 ), label = "Your Point." ) +
    guides(
      fill = "none",
      size = "none"
    ) +
    facet_wrap( explained ~ ., scales = "free" ) +
    labs(
      title = paste0( "Prediction and context ", "(", str_to_title( chosen_model_row$species ) ,")" ),
      subtitle = "How your point compares to known values.",
      x = "Variable amount.",
      y = "How many observations have this value."
    ) +
    theme_set(
      theme_gray( base_size = 30 )
    )
  return(plot)
};


## Defining API routes

app = RestRserve::Application$new(
                                ## content_type="application/json",
                                middleware = list(
                                  CORSMiddleware$new(routes = "/", match = "partial", id = "CORSMiddleware"),
                                  EncodeDecodeMiddleware$new()
                                ),
                              )

## app$add_post(path = "/qrf_predict", FUN = qrf_predict)
app$add_post(path = "/qrf_numbers", FUN = qrf_numbers)
app$add_route("/qrf_numbers", method = "OPTIONS", FUN = function(req, res) {
  res$set_header("Allow", "POST, OPTIONS")
})
app$add_post(path = "/qrf_random", FUN = qrf_random)
app$add_route("/qrf_random", method = "OPTIONS", FUN = function(req, res) {
  res$set_header("Allow", "POST, OPTIONS")
})

app$add_get(path = "/simple", FUN = simple_get )
app$add_route("/simple", method = "OPTIONS", FUN = function(req, res) {
  res$set_header("Allow", "GET, OPTIONS")
})

## app$add_route( "/qrf_numbers", method="OPTIONS", FUN = function(req,res) {
##   res$set_header( "Allow", "POST, OPTIONS" )
## } )

## app$add_get(path = "/qrf_query", FUN = qrf_query)
## app$add_post(path = "/simple_post", FUN = simple_post)

## request1 <- Request$new(
##                      path="/qrf_predict",
##                      method="POST",
##                      body = jsonlite::toJSON( testVector ),
##                      content_type = "application/json",
##                      headers=list( content_type = "application/json" )
##                    )
## response1 <- app$process_request( request1 )
## cat( "response status:", response1$status )
## cat( "response body:", response1$body )


## requestGrain <- Request$new(
##                       path="/qrf_numbers",
##                       method="POST",
##                       body = jsonlite::toJSON( testVectorGrain, dataframe = "column" ),
##                       content_type = "application/json",
##                       headers=list( content_type = "application/json" )
##                     )
## responseGrain <- app$process_request( requestGrain )
## cat( "response status:", responseGrain$status )
## cat( "response body:", responseGrain$body )

## requestCarrot <- Request$new(
##                           path="/qrf_numbers",
##                           method="POST",
##                           body = jsonlite::toJSON( failedEntry %>% as_tibble , dataframe = "column" ),
##                           content_type = "application/json",
##                           headers=list( content_type = "application/json" )
##                         )
## responseCarrot <- app$process_request( requestCarrot )
## cat( "response status:", responseCarrot$status )
## cat( "response body:", responseCarrot$body )

## requestProduce <- Request$new(
##                       path="/qrf_numbers",
##                       method="POST",
##                       body = jsonlite::toJSON( testVectorProduce, dataframe = "column" ),
##                       content_type = "application/json",
##                       headers=list( content_type = "application/json" )
##                     )
## responseProduce <- app$process_request( requestProduce )
## cat( "response status:", responseProduce$status )
## cat( "response body:", responseProduce$body )

## request3 <- Request$new(
##                       path="/simple_post",
##                       method="POST",
##                       body = jsonlite::toJSON( list( x=1, y = 2 ) ),
##                       content_type = "application/json",
##                       headers=list( content_type = "application/json" )
##                     )
## response3 <- app$process_request( request3 )
## cat( "response status:", response3$status )
## cat( "response body:", response3$body )


backend = BackendRserve$new()
backend$start(app, http_port= 8080 )

##### example query
## let datos2  = '{"whole.scan_365":[2241.51],"whole.scan_385":[2237.365],"whole.scan_450":[3127.45],"whole.scan_500":[3007.69],"whole.scan_530":[3379.7],"whole.scan_587":[4133.115],"whole.scan_632":[4532.59],"whole.scan_850":[6823.2],"whole.scan_880":[6342.76],"whole.scan_940":[9142.435],"juice.scan_365":["NA"],"juice.scan_385":["NA"],"juice.scan_450":["NA"],"juice.scan_500":["NA"],"juice.scan_530":["NA"],"juice.scan_587":["NA"],"juice.scan_632":["NA"],"juice.scan_850":["NA"],"juice.scan_880":["NA"],"juice.scan_940":["NA"],"surface.scan_365":[6125.435],"surface.scan_385":[6123.07],"surface.scan_450":[6951.545],"surface.scan_500":[6436.005],"surface.scan_530":[6810.38],"surface.scan_587":[7303.105],"surface.scan_632":[7551.92],"surface.scan_850":[9611.93],"surface.scan_880":[8918.03],"surface.scan_940":[11507.85],"Type":["wheat"],"cropColor":[null],"dataset":["c-no-vis-who"]}'

##      const url = "/qrf_numbers";

##      const request = new Request(url, {
##          method: 'POST',
##          body: datos2,
##          headers: new Headers({
##              'Content-Type': 'application/json'
##          })
##      });

##      fetch(request)
##          .then( res => res.json() )
##          .then( blob => {
##             console.log(blob)
##                       } )
##          .catch( err => {
##              console.log("Error:",err);
##          } )
##      ;

## input <- testVectorGrain
## newData <- as_tibble( input )

## namingPatternForWavelengths <- "surface.scan_"

## newData <- newData %>%
##   applyTransitionalModel( vectorFromField = ., wavelengthsGroupPattern = namingPatternForWavelengths ) %>% rename_at( vars( contains("color") ), ~ str_c( .x, "TRUE" ) )

## if ( newData$Type %in% c("oats", "wheat") ) {
##   newData <- newData %>% rename_all( ~ str_replace( .x, pattern = "surface.scan_", replacement = "whole.scan_" ) )
## }

## table <- plotPredictions( newData, plot = FALSE )
