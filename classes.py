"""Xrf Tools - Tools allowing to parse propietary binary files (PDZ files from Brucker XRF hand tracers, currently) and to predict mineral concentrations based on the scans."""
import numpy as np
import decimal
from scipy.stats import logistic
from scipy.interpolate import Akima1DInterpolator
from sklearn.linear_model import LinearRegression

def nearest( array, value, bounding_criteria ):
    """
    This function mimics an eponym one from MatLab without extreme fidelity.
    It will search inside an array for those values that are closer to a provided one.
    You can choose bounding_criteria to define open and closed intervals if needed.
    array: an array of numeric objects.
    value: a value to which the closest value will be searched for.
    bounding_criteria: Either 'previous', 'next' or 'closest'. Previous will return the index of the closest element that is less than value, 'next' will return the first value that is bigger and 'closest' will return the one with shorter distance overall.
    """
    array = np.asarray(array)
    distances = array - value
    if bounding_criteria == 'previous':
        lesser_vals = np.abs( distances[ np.where( distances <= 0 ) ] )
        assert ( len( lesser_vals ) > 0 ), "You can't use previous when no element is lesser or equal to the reference value"
        min_value = value - lesser_vals.min()
        nearest_index = np.where( array == min_value )[0].max()
        return( { 'index': nearest_index, 'value': min_value  } )
    if bounding_criteria == 'next':
        bigger_vals = np.abs( distances[ np.where( distances >= 0 ) ] )
        assert ( len( bigger_vals ) > 0 ), "You can't use next when no element is bigger or equal to the reference value"
        max_value = value + bigger_vals.min()
        nearest_index = np.where( array == max_value )[0].min()
        return( { 'index': nearest_index, 'value': max_value  } )




class InstrumentParameters:
    """
    This is the description of a particular unit of the instrument.
    Models original MATLAB function `InstrParamsTracer5651`

    model: Comercial name of the device model.
    serial_number: Individual identification of the unit to which this calibration applies.
    axis_spacing: distance between ticks on the x axis
    fwhm: function, band width
    n: function, Lorentzian:Gaussian fraction
    nominal_tube_currrent: default nominal tube current, used in normalization
    current_correction: Scaling for counts/second change deue to current.
    """
    def __init__(self,
                 model: str,
                 serial_number: str,
                 axis_spacing: float,
                 fwhm,
                 n,
                 nominal_tube_current: float,
                 current_correction
                 ):
        self.model = model
        self.serial_number = serial_number
        self.axis_spacing = axis_spacing
        self.fwhm = fwhm
        self.n = n
        self.nominal_tube_current = nominal_tube_current
        self.current_correction = current_correction

inst_params_tracer_5i = InstrumentParameters(
    model = 'Tracer 5i, Brucker',
    serial_number = 'SK5-5651',
    axis_spacing = 0.02,
    fwhm = lambda x_pos_keV: -0.00015*x_pos_keV**2 + 0.014*x_pos_keV + 0.066,
    n = lambda x_pos_keV: 10.**(-0.14*x_pos_keV-0.44),
    nominal_tube_current = 15,
    current_correction = lambda x: 0.095*x + 0.05
)

def lorentzian_function(x):
    output = 1 / ( np.array(x) * np.array(x) + 1 )
    return output

def gaussian_function(x):
    output = np.exp( - np.log(2) * np.array(x) * np.array(x) )
    return output


def pseudo_voigt_function( axis, band_center, band_fwhm, gaussian_lorentzian_ratio, band_intensity=1 ):
    # pseudoVoigtFunction generates a Pseudo-Voigt band profile.
    #
    # Spectral lines are typically Lorentzian, but instrument broadening
    # functions cause the observed lines to be shaped as a convolution of
    # Gaussian and Lorentzian functions. Calculating the convolved peaks is
    # computationally complicated. Instead, pseudo-Voight functions are used,
    # where a linear combination of Gaussian and Lorentzian peak shapes provide
    # a reasonable band shape approximation.
    #
    # Usage:
    #   [FpV] = pseudoVoigtFunction(axis, bandCenter, bandFWHM, GLratio, bandIntensity)
    #
    # Where:
    #   axis is the spectral axis
    #   bandCenter is the band center position
    #   bandFWHM is the full-width at half-max of the band
    #   GLratio is the fraction Lorentzian : Gaussian for the pV band
    #   bandIntensity (optional) sets a height. Default is 1.
    x_axis = (2 * np.array(axis) - 2 * band_center) / band_fwhm
    image = (1-gaussian_lorentzian_ratio) * gaussian_function(x_axis) + gaussian_lorentzian_ratio * lorentzian_function(x_axis)
    image = image * band_intensity
    return image


class XrfLine:
    """
    Each element on this class is a definition, used to point which sections of the axis conform the modeled xrf band. Typically an array of XrfBands will be an attribute of a BandDefinition object.
    emission_line_name: physical name of the band, for example 'N Ka1'
    band_center: peak center
    relative_intensity: coefficient in the linear combination that defines an XrfBand simulation
    simulate_line: will run an aptly parametrized pseudo voigt function. Needs to be fed with device parameters for a given scanner and an x_axis which is selected through a process each BandDefinitions object performs over the parsed_pdz file. The curved is normalized to unit area.
    """
    def __init__( self,
                  emission_line_name: str,
                  band_center: float,
                  relative_intensity: float
                  ):
        self.emission_line_name = emission_line_name
        self.band_center = band_center
        self.relative_intensity = relative_intensity
    def simulate_line(
            self,
            x_axis,
            instrument_parameters
    ):
        band_fwhm = instrument_parameters.fwhm( self.band_center )
        gaussian_lorentzian_ratio = instrument_parameters.n( self.band_center )
        image = pseudo_voigt_function( axis = x_axis,
                                       band_center = self.band_center,
                                       band_fwhm = band_fwhm,
                                       gaussian_lorentzian_ratio = gaussian_lorentzian_ratio,
                                       band_intensity = self.relative_intensity )
        return image



class XrfBand:
    """
    XRFBands relate an atomic element 'species' to several emission lines. This implies each band mentions a band and a list of XrfLine class objects.
    band_name: The name of the band, a 'species', ex: 'Fe-L'.
    xrf_lines: List of XrfLine objects. Lines can be fed either as XrfLine objects, adequately named dictionaries or adequately ordered lists. ex, for 'Fe-L' [ { 'emission_line_name': 'La1', 'band_center': 0.705, 'relative_intensity': 100 }, { 'emission_line_name': 'Lb1', 'band_center': 0.7185, 'relative_intensity': 15 } ]
    simulate_bands: Simulate all peaks implied by the lines that define the band through the lineal combination of many pseudo voigt functions. Area is normalized to unit.
    """
    def __init__( self,
            band_name: str,
            lines_list):
        assert len( lines_list ) > 0, 'At least one XrfLine is needed.'
        self.band_name = band_name
        self.xrf_lines = {}
        for line_object in lines_list:
            if type(line_object) == XrfLine:
                self.xrf_lines[line_object.emission_line_name] = line_object
            elif type(line_object) == list:
                self.xrf_lines[line_object[0]] = XrfLine( line_object[0], line_object[1], line_object[2] )
            elif type(line_object) == dict:
                self.xrf_lines[line_object[ 'emission_line_name' ]] = XrfLine( line_object['emission_line_name'], line_object['band_center'], line_object['relative_intensity'] )
    def simulate_bands(
            self,
            x_axis,
            instrument_parameters
    ):
        # each species is the combination of potentially many bands, with different relative intensities, so the simulation is the lineal combination of several pseudo voigt curves
        pure_bands = np.array([0] * len(x_axis))
        for line_name in self.xrf_lines.keys():
            line = self.xrf_lines[line_name]
            pure_bands = pure_bands + line.simulate_line( x_axis, instrument_parameters )
        pure_bands_unit_area = pure_bands / sum( pure_bands )
        return pure_bands_unit_area



class AnchorRegion:
    """
    These are regions that our calibrations detected as peak-free and are used to define the background intensities.
    start: coordinate that opens the range
    stop: coordinate that stops the range
    criteria: private variable informing how the method should be defined. Currently either 'all' (most cases) or 'min'
    get_anchor_data: will search in the axis and intensities of a parsed file for the values that are closest to the anchor description and return pairs that fulfil the criteria that the anchor object describes: either all indices in the range or just the minimum value inside the same range. The returned value is an index, not the actual axis or intensity value.
    """
    def __init__(self, start, end, criteria):
        assert start < end,  'Start should be a lower value then end'
        assert criteria in [ 'all', 'min' ],  'Region criteria should be either "all" or "min"'
        self.start = start
        self.end = end
        self.criteria = criteria
    def get_anchor_data( self, intensity, axis ):
        start_index = nearest( axis, self.start, 'next' )[ 'index' ]
        end_index = nearest( axis, self.end, 'previous' )[ 'index' ]
        local_indices = list( range( start_index, end_index + 1 ) )
        all_intensities = [ intensity[index] for index in local_indices ]
        min_index = [ local_indices[i] for i,j in enumerate( all_intensities ) if j == min( all_intensities ) ]

        if self.criteria == 'min':
            return min_index
        if self.criteria == 'all':
            return local_indices





class BackgroundRegion:
    """
    start: inferior limit of concerned region
    end: superior limit of concerned region
    degree: the degree of the interpolating polynomial
    x_axis: Once a file is read, this will contain all the x axis (abscisssa, wavelengths) coordinates of the relevant region
    y_axis: once a file is read, this will contain all the y axis (ordinate, intensities) coordinate estimations for the relevant section of the background
    """
    def __init__( self, start, end, degree, x_axis=[], y_axis=[] ):
        assert ( start < end ),  'Start should be a lower value then end'
        assert ( len( x_axis ) == len( y_axis ) ), 'Axes need to have one to one correspondence, their length should be the same'
        self.start = start
        self.end = end
        self.degree = degree
        self.x_axis=x_axis
        self.y_axis=y_axis
    def coordinates( self ):
        assert ( len( self.x_axis ) == len( self.y_axis ) ), 'Axes need to have one to one correspondence, their length should be the same'
        return [ { 'x':value, 'y': self.y_axis[index] } for index,value in enumerate(self.x_axis) ]


class BandDefinitions:
    """
    Each band definition is a set of bands, calibrations and models applied to the bands.
    This class has methods that accept a ParsedPdz and an instrument definition object and clean background noise, resample the measurements, take adequate sections for each known relevant spectral band and re models the whole spectra based on simulated pure bands and the cleand scanned intensities. The main metho, which coordinates all this functionality all the way up to a decomposition of the scanned intensities, is 'decompose_scan'.
    xrf_bands: array of XrfBand objects.
    anchor_regions: array of background AnchorRegion objects.
    truncation_limits: these are the minimum and maximum regions of the axis that are going to be processed
    background_regions: These are objects of the class BackgroundRegion
    get_anchor_data: utilizes the eponymous from the AnchorRegion class on each AnchorRegion listed in the anchor_regions attribute, obtaining an array of pixel indices defining the background.
    fit_background: Will use each backgroud region in the background_regions attribute
    subtract_background: Takes the background fitted by "fit background" and a parsed_pdz and generates a clean version of the wavelength/intensisty pairs by interpolating and subtracting it. It will work just on those sections of the x axis that exist inside the domain used to define the background. It returns both the background and a set of cleaned pairs for which the intensity is corrected.
    resample_spectrum: Resamples the spectrum through interpolation, obtaining a new curve which has a selected set of x_axis values
    get_band_simulations: get the simulations of all bands, plus potentially an aditional compton band when corresponding and a fixed constant offset band.
    decompose_scan: Takes a parsed_pdz and an instrument definition and returns a decomposition of the scan over the peaks space indicated by this band definition object's xrf_bands.
    """
    def __init__( self,
                  xrf_bands,
                  anchor_regions,
                  truncation_limits,
                  background_regions,
                  observations):
        self.xrf_bands = xrf_bands
        self.anchor_regions = anchor_regions
        self.truncation_limits = truncation_limits
        self.background_regions = background_regions
        self.observations = observations
    def get_anchor_data(
            self,
            axis,
            intensity,
    ):
        anchor_size = len( self.anchor_regions )
        pixel_indices = []
        for anchor in self.anchor_regions:
            pixel_indices = pixel_indices + anchor.get_anchor_data( axis = axis, intensity = intensity )
        return pixel_indices
    def fit_background(
            self,
            parsed_pdz
    ):
        # we are going to generate the background fitted polynomial for each background region and store them inside two array attributes ( x_axis, y_axis ) for each region
        # identifying the anchor regions, each background region will take a section of this collection
        background_indices = self.get_anchor_data( parsed_pdz.x_axis, parsed_pdz.intensity )
        # TODO defaults are obtained from OS1, should check
        current_axis = [ parsed_pdz.x_axis[i] for i in background_indices ]
        current_intensities = [ parsed_pdz.intensity[i] for i in background_indices ]
        for background_region in self.background_regions:
            # getting just the section of the background inside current region
            selected_background_region_indices = [ index for index,value in enumerate( current_axis ) if ( value >= background_region.start ) & ( value <= background_region.end ) ]
            # fitting a polynomial to the current background range
            region_background_axis = [ current_axis[i] for i in selected_background_region_indices ]
            region_background_intensities = [ current_intensities[i] for i in selected_background_region_indices ]
            region_axis = [ axis_tick for axis_tick in parsed_pdz.x_axis if ( axis_tick >= background_region.start ) & ( axis_tick <= background_region.end ) ]

            background_polynomial = np.polynomial.Polynomial.fit( region_background_axis, region_background_intensities, background_region.degree )
            estimated_region_background = [ background_polynomial(x) for x in region_axis ]
            background_region.x_axis = region_axis
            background_region.y_axis = estimated_region_background

        # the background regions need to be sewn together using logistic functions
        # TODO store logistic function values as a constant, saving the load of the package and the calcuations
        # TODO move to the previous loop, storing preceding step as constant
        # for overlapping regions, different estimates will exist and need to be merged, this is done generating an akima interpolating factor and using a convex sum
        # for non overlapping regions, the estimates are simply added. As the overlapping regions are going to be borders, interpolation is already achieved
        # the original algorithm acumulates to the left, in the end just the first element exists and it has all the steps
        merge_axis = np.arange( 0, 20 + 1 ) * 0.05
        merge_values = logistic.cdf( merge_axis, 0.5, 0.1 )
        background_pairs = []
        for index,region in enumerate( self.background_regions ):
            if len( background_pairs ) == 0:
                background_pairs.extend( region.coordinates() )
            else:
                background_pairs = join_background_regions( background_pairs, region.coordinates(), merge_axis, merge_values, left_end = self.background_regions[index-1].end, right_start = self.background_regions[index].start )
        return background_pairs
    def subtract_background(
            self,
            parsed_pdz,
            background_pairs
            ):
        background_axis = [ pair['x'] for pair in background_pairs ]
        background_intensities = [ pair['y'] for pair in background_pairs ]
        common_range = [ x_value for x_value in parsed_pdz.x_axis if ( x_value >= min(background_axis) ) & ( x_value <= max(background_axis) ) ]
        common_range_intensities = [ pair['y'] for pair in parsed_pdz.coordinates() if pair['x'] in common_range ]
        background_interpolator = Akima1DInterpolator( background_axis, background_intensities )
        interpolated_background = background_interpolator( common_range )
        clean_intensities = np.array( common_range_intensities ) - interpolated_background
        pairs = [ {'x': common_range[index], 'y':value} for index,value in enumerate(clean_intensities) ]
        return { 'cleaned_pairs': pairs, 'background':interpolated_background }
    def resample_spectrum( self,
                           pairs,
                           instrument_definition,
                           image_attribute,
                           abcissa_attribute = 'x'
                          ):
        # TODO carefuly analyze instability here, an option woul be to check for excess or lacking steps and fix the endstep
        new_x_axis = np.arange( self.truncation_limits[0], self.truncation_limits[1] + instrument_definition.axis_spacing , instrument_definition.axis_spacing )
        x_axis = [ pair[abcissa_attribute] for pair in pairs ]
        y_axis = [ pair[image_attribute] for pair in pairs ]
        akima_interpolator = Akima1DInterpolator( x = x_axis, y = y_axis )
        resampled_spectrum = [ { abcissa_attribute:x, image_attribute: akima_interpolator(x) } for x in new_x_axis ]
        return resampled_spectrum
    def intensity_correction_factor(
            self,
            parsed_pdz,
            instrument_parameters
    ):
        # divide by real time (counts/second)
        # scale to the nominal tube current (units conserved)
        # scaling function for counts/second change due to current, determined
        # empirically for the XRF spectrometer by measuring counts/second
        # against drive current and finding the line of best fit relative to the nominal drive current.
        factor = ( 1 / parsed_pdz.sample_info['Time']['TimeReal'] ) * instrument_parameters.current_correction( instrument_parameters.nominal_tube_current ) / instrument_parameters.current_correction(parsed_pdz.sample_info['TubeCurrent'])
        return factor
    def get_band_simulations(
            self,
            x_axis,
            instrument_parameters
    ):
        # notice the addition of potentially extra compton band and offset term
        # the new curves are normalized to unit area, as the others are when generated by the specialized method
        curves = []
        for band in self.xrf_bands:
            simulated_band = band.simulate_bands( x_axis, instrument_parameters )
            curves.append( { 'band_name': band.band_name, 'curve': simulated_band } )

        compton_band_is_appropiate = ( min(x_axis) <= 19.5 ) & ( max(x_axis) >= 18.5 )
        if compton_band_is_appropiate:
            compton_band = pseudo_voigt_function( x_axis, 19, 0.8, 0, 100 )
            compton_band_normalized = compton_band / sum( compton_band )
            curves.append( { 'band_name': 'Compton', 'curve': compton_band_normalized } )

        # add offset term
        offset_term = np.array( [ 1 / len(x_axis) ] * len(x_axis) )

        curves.append( { 'band_name': 'Offset', 'curve': offset_term } )
        return curves
    def decompose_scan(
            self,
            parsed_pdz,
            instrument_parameters
    ):
        fitted_background = self.fit_background( parsed_pdz )
        curve_background_subtracted = self.subtract_background( parsed_pdz, fitted_background )
        curve_resampled = self.resample_spectrum( curve_background_subtracted['cleaned_pairs'], instrument_parameters, 'y' )
        correction_factor = self.intensity_correction_factor( parsed_pdz, instrument_parameters )
        curve_corrected =  [ { 'x':pair['x'], 'y':pair['y']*correction_factor } for pair in curve_resampled ]
        simulated_bands = self.get_band_simulations( x_axis = [ pair['x'] for pair in curve_corrected ], instrument_parameters = instrument_parameters )
        y_train = [ pair['y'] for pair in curve_corrected ]
        x_train = np.array( [ band['curve'] for band in simulated_bands ] ).transpose()
        non_negative_least_squares = LinearRegression(positive=True, fit_intercept=False)
        nnls_fit = non_negative_least_squares.fit(x_train, y_train)
        coefficients = nnls_fit.coef_
        band_coefficient_pairs = [ { 'name': simulated_bands[index]['band_name'], 'band_weight': coefficients[index] } for index,value in enumerate( coefficients ) ]
        return band_coefficient_pairs


class SpeciesModelInstructions:
    """
    contains instructions needed to go from a scan already projected over a fixed base of known curves into predicted ppm contents for the mentioned element.
    the measurements list/matrix should NOT include the 1 used for an intercept. This is added through the 'use_intercept' attribute.
    """
    # TODO inform the meaning of indices and positions of all vectors
    def __init__(
            self,
            species_name,
            unit,
            index,
            band_names,
            band_indices,
            constant,
            power,
            use_intercept,
            coefficients,
            known,
            measurements,
            predictions,
            pcc,
            rmse,
            sem,
            model_version_date
    ):
        self.species_name = species_name
        self.unit = unit
        self.index = index
        self.band_names = band_names
        self.band_indices = band_indices
        self.constant = constant
        # clearly inform which power relates to each index
        # x^1 + x^2 + x^3 + 1
        # inform also the positions of the ys
        self.power = power
        self.use_intercept = use_intercept
        self.coefficients = coefficients
        self.known = known
        if use_intercept:
            #  if it's a list of scalar singletons, turn into 'matrix'/list of lists
            if type( measurements[0] != list ):
                measurements = [ [ measure, 1 ] for measure in measurements ]
            elif type( measurements[0] == list ):
                measurements = [ measure.append(1) for measure in measurements ]
        self.measurements = measurements
        self.predictions = predictions
        self.pcc = pcc
        self.rmse = rmse
        self.sem = sem
        self.model_version_date = model_version_date
    def predict_concentration(
              self,
              decomposed_scan_weights,
        ):
        # TODO doesn't seem currently prepared to deal flexibly enough with models that use more than one band, because there's not provision for polinomials with mixed terms
        measurements = []
        for model_band_name in self.band_names:
            band_weight = [ pair['band_weight'] for pair in decomposed_scan_weights if pair['name'] == model_band_name ][0]
            measurements.append(band_weight)
        assert len( measurements ) == 1, 'Currently, only single band models are supported, band_names has a length bigger than 1'
        # add powers as indicated by the power attribute in this object
        powers = np.array( range(1, self.power+1) )
        measurements = measurements ** powers
        if self.constant:
            measurements = np.concatenate( [ measurements, [1] ] )
        prediction = np.dot( measurements, self.coefficients )
        if type( prediction ) == np.ndarray:
            assert len(prediction) == 1, "Fed X matrix has wrong format, a vector is obtained in the multiplication instead of a scalar."
            prediction = prediction[0]
        return prediction



class XrfModel:
    """contains several band definitions and specieswise model definitions, which together contain enough information to feed the prediction methods
           band_definitions: a list of BandDefinition objects
           group_band_simulations_into_matrix
           non_negative_least_squares_of_intensities_over_band_simualtions:
           mineral_content_predictions
           """
    def __init__(
            self,
            species_instructions_list,
            band_definitions,
            date,
            comments = "",
         ):
             self.species_instructions_list = species_instructions_list
             self.band_definitions = band_definitions
             self.comments = comments
             self.date = date
    def get_predictions(
            self,
            # expects a dictionary of { 'name', 'band_weight' attrs }, the output of  XrbBand's decompose_scan method
            parsed_pdz,
            instrument_parameters
    ):

        # test for errors in the file attributes
        all_errors = []
        for check in file_checks_list:
            check_result = check.test_sample( parsed_pdz )
            if check_result:
                all_errors.append( check_result )

        decomposed_scan_weights = []
        for band in self.band_definitions:
            decomposed_scan_weights.extend( band.decompose_scan( parsed_pdz, instrument_parameters ) )

        for check in weight_checks_list:
            check_result = check.test_sample( decomposed_scan_weights )
            if check_result:
                all_errors.append( check_result )

        predictions = []
        for species in self.species_instructions_list:
            predictions.append(
                {
                    'Chemical Species': species.species_name,
                    'Concentration': species.predict_concentration( decomposed_scan_weights ),
                    'Concentration Unit': species.unit
                }
            )

        for check in concentration_checks_list:
            check_result = check.test_sample( predictions )
            if check_result:
                all_errors.append( check_result )

        return { 'Concentrations': predictions, 'Observations': all_errors }

error_codes = {
   '1': 'concentration out of expected range',
   '2': 'background atmospheric flush seems to be missing',
   '3': 'Lower than expected signal level',
   '4': 'Potential sample contamination',
   '5': 'Incorrect detector temperature',
   '6': 'Incorrect total measurement time',
}

class ErrorCheckParameter:
    """
    These objects are going to be called at different points during the concentration prediction process, as indicated by their `check_type` attribute.
    An object identified by having `object_id_variable_name` variable equal to object_id_value is going to be selected ( for example, if checking over the array of decomposed weights, an object can search for the entry with `band_name` equal to `Na-K` ).
    `comparison_type` indicates which check to perform over the `parameter_to_check` value, and `comparison_value` is the fixed limit against which the value is going to be compared.
    If an out of range value is detected, an error_warning_string and an error_code are going to be returned.
    """
    def __init__(
        self,
        check_type,
        object_id_value,
        comparison_type,
        comparison_value,
        error_warning_string,
        error_code
    ):
        assert check_type in [ 'Concentration', 'BandWeight', 'SampleInfo', 'SampleTime' ], 'check type should be one of Concentration, BandWeight, SampleInfo, SampleTime'
        self.check_type = check_type
        self.object_id_value = object_id_value
        self.comparison_type = comparison_type
        self.comparison_value = comparison_value
        self.error_warning_string = error_warning_string
        self.error_code = error_code

        if self.comparison_type == 'lt':
            def comparison_function( entry_value ):
                return entry_value < self.comparison_value
        if self.comparison_type == 'le':
            def comparison_function( entry_value ):
                return entry_value <= self.comparison_value
        elif self.comparison_type == 'gt':
            def comparison_function( entry_value ):
                return entry_value > self.comparison_value
        elif self.comparison_type == 'ge':
            def comparison_function( entry_value ):
                return entry_value > self.comparison_value
        elif self.comparison_type == 'eq':
            def comparison_function( entry_value ):
                return entry_value == self.comparison_value
        elif self.comparison_type == 'ne':
            def comparison_function( entry_value ):
                return entry_value != self.comparison_value

        self.comparison_function = comparison_function
        self.error_code_explanation = error_codes[str(error_code)]
    def get_test_result(
            self,
            array_of_values
    ):
        if self.check_type == 'BandWeight':
            target_entry = next(  entry for entry in array_of_values if ( entry['name'] == self.object_id_value ) )
            test_result = self.comparison_function( target_entry['band_weight'] )
            return test_result
        elif self.check_type == 'Concentration':
            target_entry = next(  entry for entry in array_of_values if ( entry['Chemical Species'] == self.object_id_value ) )
            test_result = self.comparison_function( target_entry['Concentration'] )
            return test_result
        elif self.check_type == "SampleInfo":
            test_result  = self.comparison_function( array_of_values.sample_info[self.object_id_value] )
            return test_result
        elif self.check_type == "Time":
            test_result  = self.comparison_function( array_of_values.sample_info['Time'][self.object_id_value] )
            return test_result
    def test_sample(
            self,
            array_of_values
    ):
        test_result = self.get_test_result( array_of_values )
        if test_result:
            output = {
                'ErrorString': self.error_warning_string,
                'ErrorCode': self.error_code,
                'ErrorCodeMeaning': self.error_code_explanation
            }
        else:
            output = False
        return output


error_tuples = [
        ('Concentration', 'Ni', 11, 'lt', -5, 'Ni < -5 ppm', 1),
        ('Concentration', 'Fe', 10, 'gt', 250, 'Fe > 250 ppm', 1), # soil
        ('Concentration', 'Si', 3, 'gt', 15000, 'Si > 15000 ppm', 1), # soil
        ('BandWeight', 'N-K', 1, 'gt', 1100, 'N-K > 1100 cps', 2), # missing helium atmosphere
        ('BandWeight', 'Cu-L', 5, 'lt', 10, 'Cu-L < 10 cps', 3),
        ('BandWeight', 'Mg-K', 9, 'lt', 2, 'Mg-K < 2 cps', 3),
        ('BandWeight', 'Al-K', 9, 'gt', 150, 'Al-K > 150 cps', 4), # soil ?
        ('BandWeight', 'Si-K', 13, 'gt', 750, 'Si-K > 750 cps', 4), # soil
        ('BandWeight', 'P-K', 15, 'gt', 3000, 'P-K > 3000 cps', 4), # fertilizer
        ('BandWeight', 'P-K', 15, 'lt', 50, 'P-K < 50 cps', 3),
        ('BandWeight', 'S-K', 17, 'lt', 100, 'S-K < 100 cps', 3),
        ('BandWeight', 'Cl-K', 18, 'lt', 100, 'Cl-K < 100 cps', 3),
        ('BandWeight', 'Rh-L', 19, 'lt', 1000, 'Rh-L < 1000 cps', 3),
        ('BandWeight', 'Ar-K', 20, 'lt', 200, 'Ar-K < 200 cps', 2),
        ('BandWeight', 'K-K', 21, 'lt', 1500, 'K-K < 1500 cps', 3),
        ('BandWeight', 'Ti-K', 23, 'gt', 100, 'Ti-K > 100 cps', 4), # soil
        ('BandWeight', 'Mn-K', 24, 'lt', 20, 'Mn-K < 20 cps', 3),
        ('BandWeight', 'Fe-K', 25, 'lt', 100, 'Fe-K < 100 cps', 3),
        ('BandWeight', 'Fe-K', 25, 'gt', 3000, 'Fe-K > 3000 cps', 4), # soil
        ('BandWeight', 'Co-K', 26, 'lt', 10, 'Co-K < 10 cps', 3),
        ('BandWeight', 'Ni-K', 28, 'lt', 50, 'Ni-K < 50 cps', 3),
        ('BandWeight', 'Cu-K', 29, 'lt', 15, 'Cu-K < 15 cps', 3),
        ('BandWeight', 'Br-K', 31, 'gt', 300, 'Br-K > 300 cps', 4), # agricultural chemicals & pesticides
        ('SampleInfo', 'ValidCounts', 0, 'lt', 1000000, 'valid counts < 1000000 counts', 3), # Insufficient valid counts
        ('SampleInfo', 'PulseDensity', 0, 'lt', 20000, 'pulse density < 20000 cps', 3), # Sample grinding or compression issue
        ('SampleInfo', 'DetectorTemperature', 0, 'ne', 202, 'detector temperature != 202', 5), # Detector temperature is not 202
        ('SampleTime', 'TimeTotal', 0, 'lt', 28, 'total time <28 s', 6), # Sample measurement time is not between 28 and 29 seconds
        ('SampleTime', 'TimeTotal', 0, 'gt', 29, 'total time >29 s', 6) # Sample measurement time is not between 28 and 29 seconds
]

error_objects = []
for entry in error_tuples:
    error_object = ErrorCheckParameter(
        check_type = entry[0],
        object_id_value = entry[1] ,
        comparison_type = entry[3],
        comparison_value = entry[4],
        error_warning_string = entry[5],
        error_code = entry[6]
)
    error_objects.append( error_object )

file_checks_list = [ error for error in error_objects if error.check_type in [ "SampleInfo", "SampleTime" ] ]
weight_checks_list = [ error for error in error_objects if error.check_type in [ "BandWeight" ] ]
concentration_checks_list = [ error for error in error_objects if error.check_type in [ "Concentation" ] ]



# this function is going to accept the pairs defining the regions in two BackgroundRegion objects (obtained via the 'pairs' method) , each one a list of x and y pairs. It will interpolate the y_axis values of coinciding regions using a given model curve (currently, logistic function) that is going to be adequately scaled.
def join_background_regions(
                # objects with class BackgroundRegion
        left_region_pairs,
                right_region_pairs,
                # arrays
        interpolation_axis,
                interpolation_image,
                left_end,
                right_start
        ):
    # obtain the adequate x coordinates for the interpolation, looking at the gap between the involved regions
    right_region_x_axis = [ pair['x'] for pair in right_region_pairs ]
    left_region_x_axis = [ pair['x'] for pair in left_region_pairs ]
    right_region_y_axis = [ pair['y'] for pair in right_region_pairs ]
    left_region_y_axis = [ pair['y'] for pair in left_region_pairs ]
    gap_width = left_end - right_start
    # detect the overlapping wavelengths in both regions
    common_x_values = set(left_region_x_axis).intersection(right_region_x_axis)
    shifted_interpolation_axis = np.array( interpolation_axis ) * gap_width + right_start
    akima_interpolator = Akima1DInterpolator( x = shifted_interpolation_axis, y = interpolation_image )
    region_pairs = []
    for x_value in common_x_values:
        left_region_index = left_region_x_axis.index(x_value)
        right_region_index = right_region_x_axis.index(x_value)
        akima_interpolation_factor = akima_interpolator(x_value)
        interpolated_intensity = right_region_y_axis[ right_region_index ] * akima_interpolation_factor + left_region_y_axis[ left_region_index ] * ( 1 - akima_interpolation_factor )
        region_pairs.append( { 'x': x_value, 'y': interpolated_intensity } )
        unique_region_pairs = []
    for x_value in set( left_region_x_axis ).difference(common_x_values):
            region_pairs.append( { 'x': x_value, 'y': left_region_y_axis[ left_region_x_axis.index(x_value) ]  } )
    for x_value in set( right_region_x_axis ).difference(common_x_values):
            region_pairs.append( { 'x': x_value, 'y': right_region_y_axis[ right_region_x_axis.index(x_value) ]  } )
            region_pairs.sort( key = lambda entry: entry['x'] )
    return region_pairs




xrf_bands_tuples_os3 = [
    [ 'Fe-L', [ [ 'La1', 0.705, 100 ], [ 'Lb1', 0.7185, 15 ] ] ],
    [ 'Ni-L', [ [ 'La1', 0.8515, 100 ], [ 'Lb1', 0.8688, 25.7 ] ] ],
    [ 'Cu-L', [ [ 'La1', 0.9297, 100 ], [ 'Lb1', 0.9498, 18 ] ] ],
    [ 'Na-K', [ [ 'Ka1', 1.04098, 100 ] ] ],
    [ 'Ga-K', [ [ 'Ka1', 1.09792, 100 ], [ 'Ka2', 1.09792, 10 ], [ 'Kb1', 1.1248, 30.1 ] ] ],
    [ 'Mg-K', [ [ 'Ka1', 1.24, 100 ] ] ]
]

xrf_bands_tuples_os4 = [
    [
        'N-K', [ [ 'Ka1', 0.3924, 100 ] ]
    ]
]

def tuples_to_xrf_bands( list_of_tuples ):
    xrf_band_objects = []
    for band in list_of_tuples:
        new_entry = XrfBand( band[0], band[1] )
        xrf_band_objects.append( new_entry )
    return xrf_band_objects

xrf_band_objects_os3 = tuples_to_xrf_bands( xrf_bands_tuples_os3 )
xrf_band_objects_os4 = tuples_to_xrf_bands( xrf_bands_tuples_os4 )

# Background anchor regions:
# rangeStart, rangeEnd, type
# peak-free regions based on plotting 2020 OurSci crop helium data.
anchor_region_tuples_os3 = [
    ( .74, .82, 'min' ),
    ( 1.31, 1.38, 'min' )
]

anchor_region_tuples_os4 = [
    ( 0.15, 0.25, 'all' ),
    ( 0.55, 0.65, 'all' )
]


def tuples_to_anchor_regions( list_of_tuples ):
    anchor_region_objects = []
    for anchor_region in list_of_tuples:
        current_object =  AnchorRegion(
            start = anchor_region[0],
            end = anchor_region[1],
            criteria = anchor_region[2],
        )
        anchor_region_objects.append( current_object )
    return anchor_region_objects

anchor_region_objects_os3 = tuples_to_anchor_regions( anchor_region_tuples_os3 )
anchor_region_objects_os4 = tuples_to_anchor_regions( anchor_region_tuples_os4 )


truncation_limits_os3 = [ 0.68, 1.4 ];
truncation_limits_os4 = [ 0.2, 0.6 ];

background_region_tuples_os3 = [
    ( 0.5, 1.9, 1 ),
];

background_region_tuples_os4 = [
    ( 0.1, 0.7, 1 ),
];

def tuples_to_background_regions( list_of_tuples ):
    background_region_objects = []
    for background_region in list_of_tuples:
        current_object =  BackgroundRegion(
            start = background_region[0],
            end = background_region[1],
            degree = background_region[2],
        ) 
        background_region_objects.append( current_object )
    return background_region_objects

background_region_objects_os3 = tuples_to_background_regions( background_region_tuples_os3 )

background_region_objects_os4 = tuples_to_background_regions( background_region_tuples_os4 )

band_definitions_os3 = BandDefinitions( xrf_band_objects_os3, anchor_region_objects_os3, truncation_limits_os3, background_region_objects_os3, '' )
band_definitions_os4 = BandDefinitions( xrf_band_objects_os4, anchor_region_objects_os4, truncation_limits_os4, background_region_objects_os4, '' )


xrf_band_tuples_os5 = [
    [ 'Al-K', [ ['Ka1', 1.4867, 100] ] ],
    [ 'ESCK Ka1SiKa1', [ ['ESC', 1.5738, 100] ] ],
    [ 'Si-K', [ ['Ka1', 1.73998, 100] ] ],
    [ 'ESCCaKa1SiKa1', [ ['ESC', 1.9517, 100] ] ],
    [ 'P-K', [ [ 'Ka1', 2.0137, 100 ],
             [ 'Kb1', 2.1391, 4.1 ] ] ],
    [ 'ESCCaKb1SiKa1', [ ['ESC', 2.2727, 100] ] ],
    [ 'S-K', [ [ 'Ka1', 2.30784, 100 ],
           [ 'Kb1', 2.46404, 6.5 ] ] ],
    [ 'Cl-K', [ [ 'Ka1', 2.62239, 100 ],
           [ 'Kb1', 2.8156, 8.9 ] ] ],
    [ 'Rh-L', [ [ 'La1', 2.69674, 100 ],
           [ 'La2', 2.69205, 10 ],
           [ 'Lb1', 2.83441, 42 ],
           [ 'Lb2', 3.0013, 6 ] ]
     ],
    [ 'Ar-K', [ ['Ka1', 2.9577, 100 ],
           [ 'Kb1', 3.1905, 11.3 ] ]
     ],
    [ 'K-K', [ ['Ka1', 3.3138, 100 ],
           [ 'Kb1', 3.5896, 13.7 ] ]
     ],
#     'Sn', {'La1', 3.44398, 100; # No Sn bands showing up in the calibration data.
#            'La2', 3.43542, 10;
#            'Lb1', 3.6628, 42};
    [ 'Ca-K', [ ['Ka1', 3.69168, 100 ],
           [ 'Ka2', 3.68809, 50 ],
           [ 'Kb1', 4.0127, 19.52 ] ]
     ],
    [ 'Ti-K', [ ['Ka2', 4.50486, 50 ],
           [ 'Ka1', 4.51084, 100 ],
           [ 'Kb1', 4.93181, 20.44 ] ]
     ],
    [ 'Mn-K', [ ['Ka1', 5.89875, 100 ],
           [ 'Ka2', 5.88765, 50 ],
           [ 'Kb1', 6.49045, 19.5 ] ]
     ],
    [ 'Fe-K', [ ['Ka2', 6.39084, 50 ],
           [ 'Ka1', 6.40384, 100 ],
           [ 'Kb1', 7.05798, 20.4 ] ]
     ],
    [ 'Co-K', [ ['Ka1', 6.93032, 100 ],
           [ 'Ka2', 6.9153, 50 ], 
           [ 'Kb1', 7.64943, 20 ] ]
     ],
    [ 'SUMK Ka1CaKa1', [ ['SUM', 7.0055, 100] ] ],
    [ 'Ni-K', [ ['Ka1', 7.47815, 100 ],
           [ 'Ka2', 7.46089, 50 ],
           [ 'Kb1', 8.26466, 20 ] ]
     ],
    [ 'Cu-K', [ ['Ka1', 8.04778, 100 ],
           [ 'Ka2', 8.02783, 50 ],
           [ 'Kb1', 8.90529, 20.25 ] ]
     ],
    [ 'Zn-K', [ ['Ka1', 8.63886, 100 ],
           [ 'Ka2', 8.61578, 50 ],
           [ 'Kb1', 9.572, 20 ] ]
     ],
    [ 'Br-K', [ ['Ka1', 11.9242, 100 ],
           [ 'Ka2', 11.8776, 50 ],
           [ 'Kb1', 13.2914, 15.2 ],
           [ 'Kb2', 13.4671, 2.2 ],
           [ 'Kb3', 13.2813, 8 ] ]
     ],
    [ 'Rb-K', [ ['Ka1', 13.3953, 100 ],
           [ 'Ka2', 13.3358, 50 ],
           [ 'Kb1', 14.9613, 15.8 ],
           [ 'Kb2', 15.1837, 2.6 ],
           [ 'Kb3', 14.949, 7.7 ] ]
     ],
    [ 'Sr-K', [ ['Ka1', 14.165, 100 ],
           [ 'Ka2', 14.0979, 50 ],
           [ 'Kb1', 15.8357, 16 ],
           [ 'Kb2', 16.0835, 2.8 ],
           [ 'Kb3', 15.8226, 7.7 ] ]
     ],
    [ 'Mo-K', [ ['Ka1', 17.47934, 100 ],
           [ 'Ka2', 17.3743, 50 ],
           [ 'Kb1', 19.6083, 17.1 ],
           [ 'Kb2', 19.9613, 3.6 ],
           [ 'Kb3', 19.5858, 8 ] ]
     ],
    [ 'SUMK Ka1CaKb1', [ ['SUM', 7.3265, 100] ] ]
] # unknown bands at 19.1, 20.15


anchor_region_tuples_os5 =  [
    ( 0.52, 0.65, 'all' ),
    ( 1.31, 1.38, 'all' ),
    ( 4.21, 4.35, 'all' ),
    ( 4.65, 4.85, 'all' ),
    ( 5.05, 5.55, 'all' ),
    ( 8.83, 9.34, 'all' ),
    ( 9.93, 11.5, 'all' ),
    ( 12.2, 13.1, 'min' ),
    ( 14.4, 15.4, 'all' ),
    ( 16.2, 17.1, 'all' ),
    ( 17.8, 18.4, 'all' ),
    ( 20.5, 21, 'all' ),
    ( 22, 23, 'all' ),
    ( 24, 37, 'all' ) ]

truncation_limits_os5 = [ 1.4, 40];

background_region_tuples_os5 = [
    ( -float('inf'), 0.65, 1 ),
    ( 0.52, 5.5, 3 ),
    ( 5.1, 11.3, 4 ),
    ( 10, 26, 5 ),
    ( 24,float('inf'), 1 )]



xrf_band_objects_os5 = tuples_to_xrf_bands( xrf_band_tuples_os5 )

background_region_objects_os5 = tuples_to_background_regions( background_region_tuples_os5 )

anchor_region_objects_os5 = tuples_to_anchor_regions( anchor_region_tuples_os5 )

band_definitions_os5 = BandDefinitions( xrf_band_objects_os5, anchor_region_objects_os5, truncation_limits_os5, background_region_objects_os5, '' )



na_model_instructions = SpeciesModelInstructions(
    species_name = 'Na',
    unit = 'ppm',
    index = 1,
    band_names = ['Na-K'],
    band_indices = 7,
    constant = 0,
    power = 1,
    use_intercept = False,
    coefficients = 64.0632799679,
    known = [1078,1701.2145749,1536.43724696,343,1279,932,216,305.872,1008,1150,1198,2099,1258,650,1296,149,1250,1210],
    measurements = [14.0493749437,16.7408289859,26.7588539352,0,0,23.0241445792,6.08024132225,7.17790844762,23.3569262458,4.04182678104,8.06087381736,0.0313809641576,4.04205941928,10.1325040748,14.9214580745,0,16.1403525993,21.7375848265],
    predictions = [900.049040394,1072.47241422,1714.25995127,0,0,1475.0022202,389.5202021,459.840358464,1496.32130527,258.932680656,516.406016148,2.01036749249,258.947584225,649.121445321,955.917546155,0,1034.00392735,1392.58098257],
    pcc = 0.26771,
    rmse = 732.9965,
    sem = 1.4684,
    model_version_date = 'May 2021'
)

mg_model_instructions = SpeciesModelInstructions(
    species_name = 'Mg',
    unit = 'ppm',
    index = 2,
    band_names = ['Mg-K'],
    band_indices = 9,
    constant = 1,
    power = 1,
    use_intercept = True,
    coefficients = [151.649782408,-598.805921579],
    known = [298,4000,5979,5800,5000,600,1593,1547,7864,300,7639,300,5000,3700,2027,6510,6562,8600],
    measurements = [15.8215826718,26.3400334028,39.4165162018,43.7492579564,38.4846772127,12.1338781638,15.2433104266,15.0921084083,62.2533194,4.23496237411,52.891291077,6.15866823765,34.7947906955,17.8945455641,23.6378210045,41.5202478634,42.3068444439,62.5769345717],
    predictions = [1800.53364794,3395.65441256,5378.7001837,6035.75952802,5237.38700376,1241.29406172,1712.8387878,1689.90903461,8841.89641959,43.4252009607,7422.14686152,335.154776583,4677.81651631,2114.8980195,2985.86449034,5697.73063242,5817.0178327,8890.97258996],
    pcc = 0.9656,
    rmse = 724.4449,
    sem = 0.26759,
    model_version_date = 'May 2021'
)

al_model_instructions = SpeciesModelInstructions(
    species_name = 'Al',
    unit = 'ppm',
    index = 3,
    band_names = ['Al-K'],
    band_indices = 11,
    constant = 0,
    power = 1,
    use_intercept = False,
    coefficients = 0.86661792264,
    known = [40,57,80,33.25,49,42,26,26,106,1,34,1,33,40,36,13,13,125],
    measurements = [52.8042704698,58.8180964245,66.2695210357,19.662228154,41.9503846372,52.2737624721,28.6502944781,25.3744532152,122.880739573,4.1815847211,17.3393618049,0.846162744548,44.3884047759,52.6112208041,53.2437384,17.0897703999,13.6378275304,155.544848303],
    predictions = [45.7611271811,50.9728165371,57.4303546543,17.0396393173,36.3549551883,45.3013794421,24.8288586836,21.9899559334,106.490651262,3.62383626434,15.0266017073,0.733299799896,38.4677871362,45.5938268808,46.1419779658,14.8103013224,11.8187857637,134.797953314],
    pcc = 0.95961,
    rmse = 9.6086,
    sem = 0.3082,
    model_version_date = 'May 2021'
)

si_model_instructions = SpeciesModelInstructions(
    species_name = 'Si',
    unit = 'ppm',
    index = 4,
    band_names = ['Si-K'],
    band_indices = 13,
    constant = 1,
    power = 2,
    use_intercept = True,
    coefficients = [27.0083653522,0.000826999971721,-720.748159343],
    known = [5000,8071.2068,37015.7008,0,3300,6605.6628,0,1933.8066,41900,264,23207.3458,4500,3843.6156,11919.1412,21376.8622,214.6452,1893.8106,63414],
    measurements = [ [ 232.94908599, 54265.2766635 ],
                     [ 330.201126799, 109032.78414 ],
                     [ 1337.35743768, 1788524.91612 ],
                     [ 0, 0 ],
                     [ 150.179404621, 22553.8535722 ],
                     [ 279.253372854, 77982.4462504 ],
                     [ 0, 0 ],
                     [ 87.0020449999, 7569.35583416 ],
                     [ 1512.57042256, 2287869.2832 ],
                     [ 92.6518314053, 8584.36186275 ],
                     [ 843.886023218, 712143.620184 ],
                     [ 157.221469115, 24718.5903506 ],
                     [ 172.841780435, 29874.2810639 ],
                     [ 419.880823207, 176299.905697 ],
                     [ 823.824922439, 678687.502832 ],
                     [ 79.7661607919, 6362.64040747 ],
                     [ 83.7172609607, 7008.57978277 ],
                     [ 2223.24384359, 4942813.18807 ]
                    ],
    predictions = [5615.7032458,8287.61462237,36878.2001791,-720.748159343,3354.0041053,6885.92044138,-720.748159343,1635.29471547,42023.3742666,1788.72562082,22660.1766251,3545.98899324,3972.13182437,10765.3465352,22090.6908778,1438.8673575,1546.11430625,63413.1402022],
    pcc = 0.9992,
    rmse = 694.455,
    sem = 0.043849,
    model_version_date = 'May 2021'
)


p_model_instructions = SpeciesModelInstructions( species_name = 'P',
                                                 unit = 'ppm',
                                                 index = 5,
                                                 band_names = ['P-K'],
                                                 band_indices = 15,
                                                 constant = 0,
                                                 power = 1,
                                                 use_intercept = False,
                                                 coefficients = 3.59945900424,
                                                 known = [2360,4500,5723,10879,9000,2060,6749,7970,5100,400,18400,14600,4200,4923,4592,9244,10234,6100],

                          measurements = [603.641511121,1352.31755099,1745.86046741,2861.73723849,2461.43310138,726.861835541,1935.8322309,2055.41221814,1571.17826419,82.8977024681,5104.52360608,4225.80910639,1056.01018013,1004.80902772,1534.72369378,2336.39555025,2898.21542855,1580.78509788],
                                                 predictions = [2172.78287254,4867.61158552,6284.15317956,10300.7058708,8859.82754011,2616.30937878,6967.94875422,7398.372016,5655.39175029,298.38688158,18373.5234562,15210.6266382,3801.06535142,3616.76890239,5524.17501861,8409.76000083,10432.0076205,5689.97115434],
                                                 pcc = 0.99152,
                                                 rmse = 570.2489,
                                                 sem = 0.13443,
                                                 model_version_date = 'May 2021'
                         )


s_model_instructions = SpeciesModelInstructions( species_name = 'S',
                                                 unit = 'ppm',
                                                 index = 6,
                                                 band_names = ['S-K'],
                                                 band_indices = 17,
                                                 constant = 1,
                                                 power = 2,
                                                 use_intercept = True,
                                                 coefficients = [1.54579255584,4.89101273028e-05,-445.316944814],
                                                 known = [4160,2800,3610,21770,17100,3400,2800,3400,5500,500,5500,6300,11700,2350,3100,12490,23730,5500],
                                                 measurements = [
                                                     [ 2264.59124704, 5128373.51617 ],
                                                     [ 2188.45555557, 4789337.71869 ],
                                                     [ 2596.59014516, 6742280.38192 ],
                                                     [ 10698.6226281, 114460526.139 ],
                                                     [ 9230.74205959, 85206598.9706 ],
                                                     [ 2486.48942527, 6182629.66197 ],
                                                     [ 1747.54262447, 3053905.22433 ],
                                                     [ 2552.18552165, 6513650.93691 ],
                                                     [ 3379.15858038, 11418712.7113 ],
                                                     [ 532.498362692, 283554.50627 ],
                                                     [ 3704.05227714, 13720003.2718 ],
                                                     [ 3819.70149882, 14590119.5401 ],
                                                     [ 6800.85056253, 46251568.3738 ],
                                                     [ 2006.47148924, 4025927.83715 ],
                                                     [ 2045.09205437, 4182401.51086 ],
                                                     [ 6115.08102432, 37394215.934 ],
                                                     [ 11354.9287558, 128934407.048 ],
                                                     [ 3477.82635415, 12095276.1496 ]
                                                 ],
                                                 predictions = [3306.10074841,3171.82847928,3898.23856392,21690.8131761,17990.9610184,3700.67310277,2405.38832839,3818.41593219,5336.65192607,391.68374723,5951.42659825,6172.75380165,12329.5573252,2853.18038978,2920.53291916,10836.2856426,23413.2456603,5522.26264017],
                                                 pcc = 0.99634,
                                                 rmse = 574.8185,
                                                 sem = 0.093629,
                                                 model_version_date = 'May 2021'
                         )


cl_model_instructions = SpeciesModelInstructions(
    species_name = 'Cl',
    unit = 'ppm',
    index = 7,
    band_names = ['Cl-K'],
    band_indices = 18,
    constant = 1,
    power = 1,
    use_intercept = True,
    coefficients = [1.53590238483,-453.494496831],
    known = [9000,11872,10000,310,5600,11000,1529,2261,14000,3070,0,670,2780,185,4556,1792,340,14130],
    measurements = [6309.722667581,8207.481229231,6710.776490431,530.4966419921,3311.66441611,7406.802984821,1478.052993871,1423.223978431,9539.105678821,2727.610890631,307.5471416851,802.2957279091,1803.213941831,498.2541351761,3781.438163231,1236.645953371,678.9895240231,9173.97981582],
    predictions = [9237.6235959,12152.3954966,9853.60311886,361.296560747,4632.89877762,10922.6318715,1816.65062135,1732.43860578,14197.6406644,3735.84957497,18.86789153,778.753425002,2316.06609678,311.775217636,5354.42539616,1445.87297214,589.367132389,13636.8429806],
    pcc = 0.99628,
    rmse = 424.9917,
    sem = 0.091408,
    model_version_date = 'May 2021'
)


k_model_instructions = SpeciesModelInstructions(
    species_name = 'K',
    unit = 'ppm',
    index = 8,
    band_names = ['K-K'],
    band_indices = 21,
    constant = 0,
    power = 1,
    use_intercept = False,
    coefficients = 1.1780680449,
    known = [29323,29000,19611,8857,19800,30742,13034,15999,16850,3100,13600,500,23400,28999,28057,6920,6721,17500],
    measurements = [24815.2863741,23922.5602415,15501.5504435,7730.62884684,17545.9577713,27011.6195604,11924.1295915,11913.2527767,14651.2851092,3234.26120216,11663.8475598,530.282184815,18572.3710235,24892.2752401,25237.6588151,5415.36774441,4720.24953159,13868.3446204],
    predictions = [29234.0959025,28182.4037728,18261.881224,9107.20681148,20670.3321677,31821.5258452,14047.436035,14034.6224071,17260.210804,3810.17977114,13740.8060908,624.708496713,21879.5168209,29324.7940253,29731.6793783,6379.6716911,5560.77513714,16337.853633],
    pcc = 0.99447,
    rmse = 1007.9409,
    sem = 0.11098,
    model_version_date = 'May 2021'
)


ca_model_instructions = SpeciesModelInstructions( species_name = 'Ca',
                                                  unit = 'ppm',
                                                  index = 9,
                                                  band_names = ['Ca-K'],
                                                  band_indices = 22,
                                                  constant = 1,
                                                  power = 1,
                                                  use_intercept = True,
                                                  coefficients = [1.2815840734,-546.57867393],
                                                  known = [7800,20198,29875,7903,14310,6300,1572,1274,24910,700,1900,1000,14500,14520,6609,10233,7251,27700],
                                                  measurements = [6010.93396801,14752.3480673,22778.1218964,6952.78260436,11955.4920777,5877.5136278,1407.14305405,1231.83093315,20509.7051409,845.03487405,1980.54671607,1410.10533213,13559.4830417,10712.2676154,5034.96946566,9697.52149867,5776.37493973,22113.9470993],
                                                  predictions = [7156.9385657,18359.7956543,28645.4995703,8363.99677759,14775.3895624,6985.94918262,1256.79345313,1032.11623111,25738.3327847,536.404562116,1991.658454,1260.58986154,16831.0388358,13182.0928919,5906.15800329,11881.6104302,6856.33145079,27794.3037284],
                                                  pcc = 0.99405,
                                                  rmse = 993.0647,
                                                  sem = 0.1155,
                                                  model_version_date = 'May 2021'
                         )


mn_model_instructions = SpeciesModelInstructions(
    species_name = 'Mn',
    unit = 'ppm',
    index = 10,
    band_names = ['Mn-K'],
    band_indices = 24,
    constant = 0,
    power = 1,
    use_intercept = False,
    coefficients = 0.461230744234,
    known = [214.7897,36,1722,108,2596.7906,82,24.3,228.2881,3607.5564,9,410,26,130.3529,121.0189,93.9144,86.878,118.829,3379],
    measurements = [290.974237447,137.968129992,3784.96577459,343.18307554,5566.4011307,255.510917481,95.2892462545,128.156658832,7854.17434174,72.5397304927,870.526302935,145.578249181,195.844292384,173.856162287,146.442077878,77.7635607801,96.1416480552,7325.5442617],
    predictions = [134.206264091,63.6351432768,1745.74258112,158.28658534,2567.39533622,117.84949063,43.9503299674,59.1097911318,3622.58667698,33.4575538816,401.513494578,67.145164214,90.3294087303,80.187807121,67.5435885668,35.8669450129,44.3434838843,3378.76623174],
    pcc = 0.999,
    rmse = 55.882,
    sem = 0.048686,
    model_version_date = 'May 2021'
)


fe_model_instructions = SpeciesModelInstructions( species_name = 'Fe',
                                                  unit = 'ppm',
                                                  index = 11,
                                                  band_names = ['Fe-K'],
                                                  band_indices = 25,
                                                  constant = 0,
                                                  power = 2,
                                                  use_intercept = False,
                                                  coefficients = [0.180202433331,1.82067730204e-05],
                                                  known = [123.9898,244,831,166,189.4127,180,100,159.1324,2090,167,322.7133,462,91.3891,148.1989,198.1711,78.5908,89.9794,2649],
                                                  measurements = [
                                                      [ 694.707809125, 482618.94006 ],
                                                      [ 1054.15008322, 1111232.39796 ],
                                                      [ 3378.18877842, 11412159.4227 ],
                                                      [ 828.898384513, 687072.531848 ],
                                                      [ 896.41951333, 803567.943879 ],
                                                      [ 820.316691626, 672919.47456 ],
                                                      [ 522.030898834, 272516.259337 ],
                                                      [ 721.058830761, 519925.837418 ],
                                                      [ 6945.43143576, 48239017.8288 ],
                                                      [ 733.496052909, 538016.459633 ],
                                                      [ 1575.91287674, 2483501.39508 ],
                                                      [ 2081.59548299, 4333039.7548 ],
                                                      [ 694.922888271, 482917.820644 ],
                                                      [ 842.061565129, 709067.679467 ],
                                                      [ 1127.75355813, 1271828.08786 ],
                                                      [ 527.506910088, 278263.540191 ],
                                                      [ 482.621100484, 232923.126633 ],
                                                      [ 8032.48014224, 64520737.2354 ]
                                                  ],
                                                  predictions = [133.974971155,210.192366135,816.536434403,161.878879509,176.167356748,160.074756067,99.0328799218,139.402727587,2129.86049359,141.973317134,329.199881302,453.999242548,134.019170592,164.651377347,226.379820688,100.124309913,91.2102751818,2622.18688526],
                                                  pcc = 0.9995,
                                                  rmse = 22.1368,
                                                  sem = 0.033405,
                                                  model_version_date = 'May 2021'
)


ni_model_instructions = SpeciesModelInstructions( species_name = 'Ni',
                                                  unit = 'ppm',
                                                  index = 12,
                                                  band_names = ['Ni-K'],
                                                  band_indices = 28,
                                                  constant = 1,
                                                  power = 1,
                                                  use_intercept = True,
                                                  coefficients = [0.0256103543299,-8.65583229257],
                                                  known = [4,4,3,3,2,4,5,6,1,10,6.988,13,4.787,5.342,4.831,3,4,1],
                                                  measurements = [
                                                      511.513425839 ,506.152059546 ,484.343301918 ,444.200435952 ,464.052080119 ,476.448693777 ,488.536641331 ,533.39020337 ,378.014532137 ,678.903514472 ,661.281868631 ,799.343940877 ,571.853668499 ,585.121679145 ,568.03193617 ,394.148240341 ,479.232590003 ,376.042290903
                                                  ],
                                                  predictions = [4.44420778768,4.30690129722,3.74837128688,2.72029826566,3.22870590681,3.54618757507,3.85576419505,5.00447981184,1.02525381732,8.73112726888,8.279830675,11.8156492647,5.98954278256,6.32934123644,5.89166686345,1.43844380107,3.61748414385,0.974744020478],
                                                  pcc = 0.94591,
                                                  rmse = 0.93569,
                                                  sem = 0.34411,
                                                  model_version_date = 'May 2021'
)


cu_model_instructions = SpeciesModelInstructions( species_name = 'Cu',
                                                  unit = 'ppm',
                                                  index = 13,
                                                  band_names = ['Cu-K'],
                                                  band_indices = 29,
                                                  constant = 0,
                                                  power = 1,
                                                  use_intercept = False,
                                                  coefficients = 0.0667699484064,
                                                  known = [9,7,2,5.5,11,6,9,9,2,10,21,16,5,8,10,6,5,3],
                                                  measurements = [135.567914037,114.544032245,43.0110381231,81.2705112669,149.07732322,85.140178401,107.481279648,125.516812899,23.5550776022,141.270740691,281.883848991,246.571644168,136.122187941,141.848247607,156.704976326,77.0165422283,102.461927432,32.6811319275],
                                                  predictions = [9.05186262581,7.64809912323,2.87184479639,5.42642784425,9.95388517998,5.68480531915,7.17651949677,8.38075112142,1.57277131621,9.43264006729,18.8213700537,16.4635759596,9.08887146581,9.47120017426,10.4631831843,5.14239055103,6.84137760827,2.18211749266],
                                                  pcc = 0.95311,
                                                  rmse = 1.4034,
                                                  sem = 0.31295,
                                                  model_version_date = 'May 2021'
)


zn_model_instructions = SpeciesModelInstructions( species_name = 'Zn',
                                                  unit = 'ppm',
                                                  index = 14,
                                                  band_names = ['Zn-K'],
                                                  band_indices = 30,
                                                  constant = 0,
                                                  power = 1,
                                                  use_intercept = False,
                                                  coefficients = 0.304178427096,
                                                  known = [37.0768,25.2,24,223,271,30.5,58.7,76,29.1344,4,230,83,26.0784,37.0512,43.2288,37.152,51.1488,35],
                                                  measurements = [121.182661038,96.545427279,66.1284644206,721.313776273,871.810657253,121.818430961,207.409923537,260.770181675,81.8549166277,15.6690362846,763.097615803,303.292439523,113.950511325,114.793112648,177.646748911,124.100180209,147.251133986,75.0307685713],
                                                  predictions = [36.8611512258,29.367036213,20.1148522937,219.408089909,265.185994449,37.0545387212,63.0896243055,79.3206636955,24.8984997899,4.76618281116,232.117832496,92.2550172043,34.6612873018,34.9175884467,54.0363086624,37.7485976182,44.7906183239,22.8227411678],
                                                  pcc = 0.9971,
                                                  rmse = 5.9871,
                                                  sem = 0.079271,
                                                  model_version_date = 'May 2021'
)


mo_model_instructions = SpeciesModelInstructions( species_name = 'Mo',
                                                  unit = 'ppm',
                                                  index = 15,
                                                  band_names = ['Mo-K'],
                                                  band_indices = 34,
                                                  constant = 0,
                                                  power = 1,
                                                  use_intercept = False,
                                                  coefficients = 0.193045417,
                                                  known = [0.8,0,2,0.2,1,4,0,0.24713968,5,0,4,2,3,1,9,0.05,2,2],
                                                  measurements = [3.19331691034,2.11112692701,0,0.96235224442,13.7694117082,6.38124605666,0.813666638275,0,0,0,0.130247893571,0,0,0,0,0,6.03845343577,0],
                                                  predictions = [0.616455194569,0.407543377965,0,0.185777690325,2.65812182504,1.23187030599,0.157074615484,0,0,0,0.0251437589278,0,0,0,0,0,1.16569576154,0],
                                                  pcc = -0.088869,
                                                  rmse = 2.9363,
                                                  sem = 1.3325,
                                                  model_version_date = 'May 2021'
)


may2021_species_instructions = [
    na_model_instructions,
    mg_model_instructions,
    al_model_instructions,
    si_model_instructions,
    p_model_instructions,
    s_model_instructions,
    cl_model_instructions,
    k_model_instructions,
    ca_model_instructions,
    mn_model_instructions,
    fe_model_instructions,
    ni_model_instructions,
    cu_model_instructions,
    zn_model_instructions,
    mo_model_instructions,
]

may2021_model = XrfModel(  may2021_species_instructions, [ band_definitions_os3, band_definitions_os4, band_definitions_os5 ], "May 2021" )
