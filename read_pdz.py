import struct
import numpy as np

filename = '3275.pdz'

struct_format_utils = {
    'Q':{ 'symbol': 'Q', 'length': 8, 'mathlab_name': 'uint32', 'natural_name':'unsigned long long' },
    'L':{ 'symbol': 'L', 'length': 8, 'mathlab_name': 'uint16', 'natural_name':'unsigned long integer' },
    'H':{ 'symbol': 'H', 'length': 2, 'mathlab_name':'uint8', 'natural_name':'unsigned short integer' },
    'd':{ 'symbol': 'd', 'length': 8, 'mathlab_name':'float64', 'natural_name':'double float' },
    'f':{ 'symbol': 'f', 'length': 4, 'mathlab_name':'float32', 'natural_name':'simple float' },
    's':{ 'symbol': 'f', 'length':1, 'mathlab_name':'char', 'natural_name':'char' },
    'B':{ 'symbol': 'B', 'length':1, 'mathlab_name':'uint8', 'natural_name': 'integer' },
    'I':{ 'symbol': 'I', 'length':4, 'mathlab_name':'uint32', 'natural_name': 'unsigned integer' }
}


def feed_num_pixel( num_pixel ):
    """
    The parser is designed to work a variable at a time, but the attribute 'intensity data' requires information about num pixel. This function will adequately modify the definition in the parsing instructions array based on the file.
    """
    index = 0
    found = False
    while not found:
        # print(index)
        if file_format_positions[index]['attribute_name'] == 'IntensityData':
            found = True
        else:
            index = index + 1

    file_format_positions[index]['datum_length']  = num_pixel
    return num_pixel


# this array describes how to read each known section of the file
file_format_positions = [
    { 'attribute_name':'NumPixels', 'offset':6, 'struct_type_symbol': 'L', 'procedure': feed_num_pixel },
            { 'attribute_name':'DetectorTemperature' , 'offset':30, 'struct_type_symbol': 'B', 'type_name': 'uint8' },
            { 'attribute_name':'AmbientTemperature' , 'offset':32, 'struct_type_symbol': 'L', 'procedure': lambda raw_value: raw_value / 10 },
            { 'attribute_name':'XLinearStep' , 'offset':50, 'struct_type_symbol': 'd' },
            { 'attribute_name':'XOffset' , 'offset':58, 'struct_type_symbol': 'd' },
            { 'attribute_name':'Year' , 'offset':146, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'Month' , 'offset':148, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'Day' , 'offset':152, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'Hour' , 'offset':154, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'Minute' , 'offset':156, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'Second' , 'offset':158, 'struct_type_symbol': 'H', 'family':'Date' },
            { 'attribute_name':'HighVoltage' , 'offset':162, 'struct_type_symbol': 'f' },
            { 'attribute_name':'TubeCurrent' , 'offset':166, 'struct_type_symbol': 'f' },
            { 'attribute_name':'HDVAC' , 'offset':170, 'struct_type_symbol': 'H' },
            { 'attribute_name':'FCDAC' , 'offset':171, 'struct_type_symbol': 'H' },
            { 'attribute_name':'PulseLength' , 'offset':173, 'struct_type_symbol': 'H' },
            { 'attribute_name':'PulseCurrent' , 'offset':174, 'struct_type_symbol': 'H' },
            { 'attribute_name':'FilterIndex' , 'offset':175, 'struct_type_symbol': 'H' },
            { 'attribute_name':'SerialNumber' , 'offset':186, 'struct_type_symbol': 's', 'datum_length': 8, 'procedure': lambda raw_bytes: raw_bytes[0].decode() },
            { 'attribute_name':'RawCountsLP' , 'offset':202, 'struct_type_symbol': 'I' },
            { 'attribute_name':'ValidCountsLP' , 'offset':206, 'struct_type_symbol': 'I' },
            { 'attribute_name':'RawCounts' , 'offset':322, 'struct_type_symbol': 'I' },
            { 'attribute_name':'ValidCounts' , 'offset':326, 'struct_type_symbol': 'I' },
            { 'attribute_name':'TimeTotal' , 'offset':326, 'struct_type_symbol': 'f', 'family':'Time' },
            { 'attribute_name':'TimeReal' , 'offset':342, 'struct_type_symbol': 'f', 'family':'Time' },
            { 'attribute_name':'TimeUNK1' , 'offset':346, 'struct_type_symbol': 'f', 'family':'Time' },
            { 'attribute_name':'TimeUNK2' , 'offset':350, 'struct_type_symbol': 'f', 'family':'Time' },
            { 'attribute_name':'TimeLive' , 'offset':354, 'struct_type_symbol': 'f', 'family':'Time' },
            { 'attribute_name':'IntensityData' , 'offset':358, 'struct_type_symbol': 'I', 'datum_length': 0 },
        ]

# some fixed operations performed after reading the binary file
def format_spectral_read( file_info ):
    file_info[ 'PulseDensity' ] = file_info[ 'ValidCounts' ] / file_info['Time'][ 'TimeLive' ]
    file_info[ 'xlabel' ] = 'Energy'
    file_info[ 'xunits' ] = 'KeV'
    file_info[ 'ylabel' ] = 'Intensity'
    file_info[ 'yunits' ] = 'Counts'
    return file_info

def generate_x_axis( file_info ):
    x_axis = ( file_info[ 'XOffset' ] + np.array( range( 0, file_info[ 'NumPixels' ] ) ) * file_info[ 'XLinearStep' ] ) / 1000
    return x_axis

# probably should change to provide filename and parser be a method or even an attribute
class ParsedPdz:
    """
    We need to store the contents of the parsed binary file in an accesible format to perform the calculations.
    intensity:
    x_axis:,
    sample_info:
    """
    def __init__(
            self,
            intensity,
            x_axis,
            sample_info
    ):
        self.intensity = intensity
        self.x_axis = x_axis
        self.sample_info = sample_info
    def coordinates( self ):
        assert ( len( self.x_axis ) == len( self.intensity ) ), 'Axes need to have one to one correspondence, their length should be the same'
        return [ { 'x': value, 'y': self.intensity[index] } for index,value in enumerate(self.x_axis) ]


def parser(filename):
    file_info = { 'FileName': filename, 'Time': {}, 'Date': {} }
    with open(filename, 'rb') as pdz_file:

        for file_attribute in file_format_positions:
            pdz_file.seek( file_attribute['offset'] )
            if 'datum_length' in file_attribute:
                struct_format_string = '{0}{1}'.format(file_attribute['datum_length'], file_attribute[ 'struct_type_symbol' ])
                struct_size = struct.calcsize( struct_format_string )
                attribute_binary_value = pdz_file.read( struct_size )
                attribute_parsed_value = struct.unpack( struct_format_string , attribute_binary_value)
            else:
                attribute_binary_value = pdz_file.read( struct_format_utils[ file_attribute['struct_type_symbol'] ]['length'] )
                attribute_parsed_value = struct.unpack( file_attribute[ 'struct_type_symbol' ], attribute_binary_value)[0]

            if 'procedure' in file_attribute:
                attribute_parsed_value = file_attribute['procedure'](attribute_parsed_value)

            # write, checking if a sub path is needed
            if 'family' in file_attribute:
                file_info[ file_attribute['family'] ][ file_attribute['attribute_name'] ] = attribute_parsed_value
            else:
                file_info[ file_attribute['attribute_name'] ] = attribute_parsed_value

        intensity_data = np.array( file_info['IntensityData'] )
        file_info = format_spectral_read(file_info)
        del file_info['IntensityData']
        x_axis = generate_x_axis( file_info )
        output_object = ParsedPdz( intensity_data, x_axis, file_info )
        # return( { 'IntensityData': intensity_data, 'XAxis': x_axis, 'FileInfo':file_info } )
        return( output_object )

parsedFile = parser(filename)

# # fseek(fileID, 30, 'bof'); % Skip to Detector Temperature (in counts?)
# # DetectorTemperature = fread(fileID,1,'uint8');
# # FileInfo.DetectorTemperature = DetectorTemperature;
# # DetectorTemperature = 202
# with open(filename, 'rb') as pdz_file:
#     pdz_file.seek( 30 )
#     struct_format_string = 'B'
#     struct_size = struct.calcsize( struct_format_string )
#     attribute_binary_value = pdz_file.read( struct_size )
#     print( attribute_binary_value  )
#     attribute_parsed_value = struct.unpack( struct_format_string , attribute_binary_value)
#     print( attribute_parsed_value )


# # fseek(fileID, 358, 'bof'); % Skip to Spectral Data
# # IntensityData = fread(fileID,NumPixels,'uint32');
# with open(filename, 'rb') as pdz_file:
#     pdz_file.seek( 358, 0 )
#     struct_format_string = '2048I'
#     struct_size = struct.calcsize( struct_format_string )
#     attribute_binary_value = pdz_file.read( struct_size )
#     attribute_parsed_value = struct.unpack( struct_format_string , attribute_binary_value)
#     print( attribute_parsed_value )
